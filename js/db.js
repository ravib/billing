// Modify accordingly
var DB_URL = "http://localhost:8529"
var RootCredentials = {
    username: "root",
    passwd: "password"
}
var Credentials = {
    username: "root", // TODO(bhoot) fixthis
    passwd: "password"
};

var DBName = "bills" // Update this

var CachedDB;

async function getDB() {
    // Returns the DB object.
    // Initializes it if required

    // Check in cache first
    if (CachedDB) return CachedDB;


    CachedDB = new arangojs.Database({
        url: DB_URL
    });

    // check if DB exists
    CachedDB.useBasicAuth(RootCredentials.username, RootCredentials.passwd);
    CachedDB.useDatabase("_system");
    var databases = await CachedDB.listDatabases();
    if (databases.indexOf(DBName) == -1) {
        console.log("Database not found " + DBName + ". Creating DB");
        await CachedDB.createDatabase(
            DBName,
            [Credentials]
        );
    }
    CachedDB.useBasicAuth(Credentials.username, Credentials.passwd);
    CachedDB.useDatabase(DBName);
    return CachedDB;
}

async function testDB() {
    // Tests the database connection
    // prints the collections in the default DB - `DBName`
    db = await getDB();
    console.log("Your collections: ");
    var dbNames = (
        await db.listCollections()
    ).map(c => { return c.name });
    console.log(dbNames);
}
